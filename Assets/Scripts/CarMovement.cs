﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(Rigidbody))]
public class CarMovement : MonoBehaviour
{
    public GameObject collisionEffect;
    public GameObject explosionEffect;
    public GameObject deadEffect;
    
    public float maxForce;
    private float m_CurrentForce;
    private Vector3 m_TrueForward;

    private Rigidbody m_RigidBody;
    
    // Start is called before the first frame update
    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        StartCoroutine(HandleDeath());
    }

    private IEnumerator HandleDeath()
    {
        while (true)
        {
            var pos = transform.position;
            
            if (transform.rotation.x > 0.5 || transform.rotation.x < -0.5)
            {
                Instantiate(explosionEffect, pos, UnityEngine.Random.rotation);
                Instantiate(deadEffect, pos, UnityEngine.Random.rotation);
                Destroy(gameObject);

                GameManager.lose = true;
                yield return null;
            }
            
            yield return new WaitForSeconds(0.5f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
        //--------------------------------------------------------------------------------------------
        // -- Only Android Code.
        // -------------------------------------------------------------------------------------------
            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                if (m_CurrentForce < maxForce)
                        m_CurrentForce += 500.0f;
            }
            else
            {
                if (m_CurrentForce > 0.0f)
                    m_CurrentForce -= 100.0f;
            }
        #else
            if (Input.GetAxis("Vertical") > 0 && m_RigidBody.detectCollisions.Equals(true))
            {
                if (m_CurrentForce < maxForce)
                    m_CurrentForce += 500.0f;
            }
            else
            {
                if (m_CurrentForce > 0.0f)
                    m_CurrentForce -= 100.0f;
            }
                
        #endif
        
        GameManager.speed = m_RigidBody.velocity.magnitude;
    }

    private void FixedUpdate()
    {        
        m_TrueForward = transform.rotation * Vector3.right;
        m_RigidBody.AddForce(m_TrueForward * m_CurrentForce, ForceMode.Force);  
        
        #if UNITY_ANDROID && !UNITY_EDITOR
            // Rotation with accelerometer.
            if (m_CurrentForce > 0.0f)
                gameObject.transform.Rotate(new Vector3(0, 1, 0) * (Time.deltaTime * m_CurrentForce/500 * Input.acceleration.x));
        #else
            if (m_CurrentForce > 0.0f)
                gameObject.transform.Rotate(new Vector3(0, 1, 0) * (Time.deltaTime * m_CurrentForce/500 * Input.GetAxis("Horizontal")));
        #endif
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.CompareTag("ground"))
        {
            m_CurrentForce -= other.impulse.magnitude;

            if (m_CurrentForce < 0.0f)
                m_CurrentForce = 0.0f;
            
            StartCoroutine(HandleCollision(other));
        }
    }

    private IEnumerator HandleCollision(Collision coll)
    {
        GameObject effect = Instantiate(collisionEffect,transform.position,UnityEngine.Random.rotation);
        yield return new WaitForSeconds(0.5f);
        
        Destroy(effect);
        yield return null;
    }
}