﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceCar : MonoBehaviour
{
    public Text textLoser;
    public Text textSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        textLoser.text = "";
    }

    public void Click()
    {
        GameManager.lose = false;
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
    
    // Update is called once per frame
    void Update()
    {   
        if (GameManager.lose)
            textLoser.text = "Game Over";
        else
        {
            textSpeed.text = "Speed: " + GameManager.speed + " km/h";
        }
    }
}
