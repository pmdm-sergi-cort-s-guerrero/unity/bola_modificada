﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movement : MonoBehaviour
{
    public float jumpValue;
    public float forceValue;
    private Rigidbody _rigidbody;
    private AudioSource _audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
//        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        /* Movimiento cinematico. */
//        transform.Translate(Input.GetAxis("Horizontal")*speed*Time.deltaTime,
//                            0,
//                            Input.GetAxis("Vertical")*speed*Time.deltaTime);

        // Jump - PC
//        if (Input.GetButtonDown("Jump") && Math.Abs(rigidbody.velocity.y) < 0.01f)
//        {
//            rigidbody.AddForce(Vector3.up*jumpValue, ForceMode.Impulse);
//            audioSource.Play();
//        }
        
//        // Jump - Mobile
//        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began &&
//            Math.Abs(rigidbody.velocity.y) < 0.01f)
//        {
//            rigidbody.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
//            audioSource.Play();
//        }
//        else 
//        {
//            if (Input.deviceOrientation == DeviceOrientation.FaceUp && Input.acceleration.z >= 0.5f &&
//                Math.Abs(rigidbody.velocity.y) < 0.01f)
//            {
//                rigidbody.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
//                audioSource.Play();
//            }
//        }
    }

    void FixedUpdate()
    {
        // Keys movement. (PC)
        _rigidbody.AddForce(new Vector3(Input.GetAxis("Horizontal"),
                                        0,
                                        Input.GetAxis("Vertical")) * forceValue);
        
        // Accelerometer movement. (Mobile)
        _rigidbody.AddForce(new Vector3(Input.acceleration.x,
                               0,
                               Input.acceleration.y) * forceValue);
    }

//    private void OnCollisionEnter(Collision other)
//    {
//        if (other.gameObject.CompareTag("enemy"))
//        {
//            print("Collision detected");
////            Destroy(other.gameObject);
//        }
//    }

//    private void OnTriggerEnter(Collider other)
//    {
//        print("You have entered the dark zone!.");
//    }
}