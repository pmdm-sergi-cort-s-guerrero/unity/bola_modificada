﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public GameObject target;
    private Vector3 m_Offset1;
    
    // Start is called before the first frame update
    void Start()
    {
        m_Offset1 = transform.position - target.transform.position;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        transform.position = target.transform.position + m_Offset1;
    }
}
