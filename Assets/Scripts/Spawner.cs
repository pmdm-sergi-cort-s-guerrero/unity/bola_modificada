﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject stones;
    public float fireRate = 0.5f;
//    private float nextFire = 0.0f;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ThrowStone());
    }

    private IEnumerator ThrowStone()
    {
        yield return new WaitForSeconds(2.0f);
        
        while (true)
        {
            Instantiate(stones, transform.position, Random.rotation);
            yield return new WaitForSeconds(fireRate);
        }
    }

    // Update is called once per frame
    void Update()
    {
//        if (Time.time > nextFire)
//        {
//            nextFire = Time.time + fireRate;
//            Instantiate(stones, transform.position, Random.rotation);
//        }
    }
}
